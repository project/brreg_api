<?php

namespace Drupal\Tests\brreg_api\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the default scheduler plugins.
 *
 * @group brreg_api
 */
class BrregApiIntegrationTest extends KernelTestBase implements ServiceModifierInterface {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['brreg_api'];

  /**
   * Test that we can look up an actual company.
   *
   * We are using the Norwegian Drupal Association for this. Let's hope it is
   * not dissolved any time soon🤞️.
   *
   * @dataProvider getNumbers
   */
  public function testLookupCompany($number) {
    /** @var \Drupal\brreg_api\BrregClient $service */
    $service = $this->container->get('brreg_api.client');
    $company = $service->getCompany($number);
    $this->assertNotFalse($company);
  }

  /**
   * Test company subdivisions search.
   *
   * @dataProvider getNumbers
   */
  public function testCompanySubdivisions($number) {
    /** @var \Drupal\brreg_api\BrregClient $service */
    $service = $this->container->get('brreg_api.client');
    $company = $service->getSubdivisions($number);
    $this->assertNotFalse($company);
  }

  /**
   * Test search by company name.
   */
  public function testCompanySearchByName() {
    /** @var \Drupal\brreg_api\BrregClient $service */
    $service = $this->container->get('brreg_api.client');
    $company = $service->getByName('DRUPAL NORGE');
    $this->assertNotFalse($company);
  }

  /**
   * A data provider.
   */
  public function getNumbers() {
    return [
      ['996 299 155'],
      [996299155],
    ];
  }

  /**
   * Alter the container.
   *
   * @todo: Remove once https://www.drupal.org/project/drupal/issues/2571475 is
   * commited.
   */
  public function alter(ContainerBuilder $container) {
    $container->removeDefinition('test.http_client.middleware');
  }

}
