<?php

namespace Drupal\brreg_api;

use GuzzleHttp\ClientInterface;

/**
 * BrregClient service.
 */
class BrregClient {

  const API_BASE = 'https://data.brreg.no/enhetsregisteret/api';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a BrregClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * Get a company by its number.
   *
   * @see https://data.brreg.no/enhetsregisteret/api/docs/index.html#enheter-oppslag
   */
  public function getCompany($number) {
    $number = $this->processOrgNumber($number);
    return $this->doRequest('enheter/' . $number);
  }

  /**
   * Get company subdivisions.
   *
   * @see https://data.brreg.no/enhetsregisteret/api/docs/index.html#_eksempel_1_hent_underenhet
   */
  public function getSubdivisions($number) {
    $number = $this->processOrgNumber($number);
    return $this->doRequest('underenheter', ['overordnetEnhet' => $number]);
  }

  /**
   * Search company by name.
   */
  public function getByName(string $name) {
    return $this->doRequest('enheter', ['navn' => $name]);
  }

  /**
   * Do all of the requesting.
   */
  protected function doRequest($path, $parameters = []) {
    $url = sprintf('%s/%s', self::API_BASE, $path);
    $response = $this->httpClient->request('GET', $url, [
      'query' => $parameters,
      'headers' => [
        'User-Agent' => 'Drupal Brreg API client (https://www.drupal.org/project/brreg_api)',
      ],
    ]);
    if ($response->getStatusCode() != 200) {
      throw new \Exception('Unexpected status code: ' . $response->getStatusCode());
    }
    $json = @json_decode($response->getBody());
    if (empty($json)) {
      throw new \Exception('Empty JSON response from Brreg API');
    }
    return $json;
  }

  /**
   * Process organization number.
   */
  protected function processOrgNumber($number) {
    // Number could be 123 321 123 or 123123123.
    return preg_replace('/[^0-9]/', '', $number);
  }

}
